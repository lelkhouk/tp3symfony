<?php

namespace App\DataFixtures;

use App\Entity\Spectacle;
use App\Entity\Lieu;
use App\Entity\Participant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // ajout jeux de données pour les lieux
        $villes = ['Paris','Bordeaux','La Rochelle','Niort','Sainte'];
        $lieux = [];

        for ($i = 0; $i <= 20; $i++) {
            $lieu = new Lieu();
            $lieu->setNom('lieu_'. $i);
            $lieu->setAdresse('adresse du lieu_'. $i);

            $randomVille = array_rand($villes);
            $lieu->setVille($villes[$randomVille]. $i);

            $lieu->setCodePostal(str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT));

            $lieux[] = $lieu;
            $this->addReference('lieu_' . $i, $lieu);
        
            $manager->persist($lieu);
        }

        // ajout jeux de données pour les participants
        $noms= ['Didier','Durand','Dubois','Dupont','Delacourt'];
        $prenoms=['Marie','Nicole','Arthur','Jean','Marc'];
        $emails=['@gmail.com','@yahoo.fr','@hotmail.com'];
        $participants = [];
       
        function generateRandomPassword($length = 12) {
            $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[]{}|;:,.<>?';
            $charsLength = strlen($chars);
        
            $password = '';
            for ($i = 0; $i < $length; $i++) {
                $password .= $chars[random_int(0, $charsLength - 1)];
            }
        
            return $password;
        }
        
        // Utilisation


        for($i=1;$i<=20;$i++){
            $participant = new Participant();

            $randomNom = array_rand($noms);
            $randomPrenom = array_rand($prenoms);
            $randomEmails = array_rand($emails);
            $randomPassword = generateRandomPassword();


            $participant->setNom($noms[$randomNom]);
            $participant->setPrenom($prenoms[$randomPrenom]);
            $participant->setAdresseEmail($prenoms[$randomPrenom].'_'.$noms[$randomNom].$emails[$randomEmails]);
            // ajout mot de passe et roles : 
            $participant->setRoles(['ROLE_USER']);
            $participant->setPassword(password_hash($randomPassword, PASSWORD_BCRYPT));

            $participants[]=$participant;
            $this->addReference('participant_' . $i, $participant);
            $manager->persist($participant);

        }

    


        for ($i = 1; $i <= 20; $i++) {
            $spectacle = new Spectacle();
            $spectacle->setNom('spectacle_'. $i);
            $spectacle->setDescription('description du spectale_'. $i);
            $spectacle->setDate(new \DateTime());
            $spectacle->setImage('images/'. $i.'.jpg');

            // ajout lieu
            $randomLieu = $this->getReference('lieu_' . array_rand($lieux));  
            $spectacle->setLieu($randomLieu);

            
          
                $randomParticipants = array_rand($participants, 2);

                foreach ($randomParticipants as $randomParticipantIndex) {
                    $p = $participants[$randomParticipantIndex];
                    $spectacle->addParticipant($p);
                }

            
            $manager->persist($spectacle);
        }
        $manager->flush();
    }
}
