<?php

namespace App\Entity;

use App\Repository\LieuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LieuRepository::class)]
class Lieu
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nom = null;

    #[ORM\Column(length: 255)]
    private ?string $adresse = null;

    #[ORM\Column(length: 5)]
    private ?string $codePostal = null;

    #[ORM\Column(length: 255)]
    private ?string $ville = null;

    #[ORM\OneToMany(mappedBy: 'lieu', targetEntity: Spectacle::class)]
    private Collection $spectacles;

    public function __construct()
    {
        $this->nom = null;
        $this->adresse = null;
        $this->codePostal = null;
        $this->ville = null;
        $this->spectacles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): static
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(string $codePostal): static
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): static
    {
        $this->ville = $ville;

        return $this;
    }
    public function __toString(): string{
        return sprintf(" ", $this->getVille(), $this->getNom(),$this->getAdresse(),$this->getCodePostal());
    }

    /**
     * @return Collection<int, Spectacle>
     */
    public function getSpectacles(): Collection
    {
        return $this->spectacles;
    }

    public function addSpectacle(Spectacle $spectacle): static
    {
        if (!$this->spectacles->contains($spectacle)) {
            $this->spectacles->add($spectacle);
            $spectacle->setLieu($this);
        }

        return $this;
    }

    public function removeSpectacle(Spectacle $spectacle): static
    {
        if ($this->spectacles->removeElement($spectacle)) {
            // set the owning side to null (unless already changed)
            if ($spectacle->getLieu() === $this) {
                $spectacle->setLieu(null);
            }
        }

        return $this;
    }
}
