<?php

namespace App\Controller;

use App\Entity\Spectacle;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\SpectacleRepository;
use App\Repository\LieuRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\FormType;
use DateTime;

class DefaultController extends AbstractController
{

    public function __construct(private spectacleRepository $spectacleRepository, private lieuRepository $lieuRepository)
    {
    }

    #[Route('/', name: 'app_homepage')]
    public function index(): Response
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }
    #[Route('/test/validation', name: 'app_test')]
    public function test(): Response
    {
        
        $dateString = '2023-11-09';
        $date = DateTime::createFromFormat('Y-m-d', $dateString);
        $unSpectacle= new Spectacle('un spectacle','description','images/1.jpg',$date);
        return $this->render('default/test.html.twig',['unSpectacle'=>$unSpectacle]);
    }

    #[Route('/spectacles', name: 'app_spectacles')]
    public function spectacles(): Response
    {
        $spectacles = $this->spectacleRepository->findAll();
        return $this->render('default/spectacles.html.twig',['spectacles'=>$spectacles]);
    }
    #[Route('/lieux', name:'app_lieux')]
    public function lieux(): Response
    {
        $lieux = $this->lieuRepository->findAll();
        return $this->render('default/lieux.html.twig',['lieux'=>$lieux]);
    }

    #[Route('/spectacle/ajout', name:'app_ajout')]
    public function AjoutLieu(Request $request,EntityManagerInterface $entityManager): Response{
        // on fait appel a la class
        $spectacle = new Spectacle('Nom par défaut', 'Description par défaut', 'images/1.jpg', new \DateTime());

        //on fait appel au formulaire
        $form = $this->createForm(FormType::class, $spectacle);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) { //si les infos sont valide et qu'on clique sur le boutton "envoyer"
            
            // Traitez les données du formulaire ici
            $entityManager->persist($spectacle);
            $entityManager->flush(); 
           

            $this->addFlash('success', 'levenement a bien ete ajouter !');
        }

        return $this->render('default/ajoutForm.html.twig',[ 'ajoutForm' => $form->createView()]);

    }
}
